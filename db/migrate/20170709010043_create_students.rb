class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :cedula
      t.string :nombre
      t.string :apellido
      t.date :fecha_nac
      t.string :genero
      t.references :school, foreign_key: true

      t.timestamps
    end
  end
end
