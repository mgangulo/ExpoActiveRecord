class RemoveNotaToStudentSubjects < ActiveRecord::Migration[5.0]
  def change
    remove_column :student_subjects, :nota, :string
  end
end
