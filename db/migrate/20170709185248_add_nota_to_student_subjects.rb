class AddNotaToStudentSubjects < ActiveRecord::Migration[5.0]
  def change
    add_column :student_subjects, :nota, :integer
  end
end
