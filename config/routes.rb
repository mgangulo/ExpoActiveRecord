Rails.application.routes.draw do
  resources :student_subjects
  resources :subjects
  resources :students
  resources :schools
  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
