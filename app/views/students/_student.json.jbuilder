json.extract! student, :id, :cedula, :nombre, :apellido, :fecha_nac, :genero, :school_id, :created_at, :updated_at
json.url student_url(student, format: :json)
