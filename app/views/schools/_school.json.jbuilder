json.extract! school, :id, :nombre, :direccion, :created_at, :updated_at
json.url school_url(school, format: :json)
