class School < ApplicationRecord
	has_many :students

	validates :nombre,
            presence: { message: "Introduzca el nombre del colegio" },
			format: {with: /\A([A-Za-z0-9ñÑ\-\.\ ]+)\z/, message: "Caracteres invalidos"}
 	
 	validates :direccion,
            presence: { message: "Introduzca la direccion del colegio" },
			format: {with: /\A([A-Za-z0-9ñÑ\-\.\ ]+)\z/, message: "Caracteres invalidos"}
end
