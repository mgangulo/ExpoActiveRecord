class StudentSubject < ApplicationRecord
  belongs_to :student
  belongs_to :subject

  	validates :nota,
			      numericality: {only_integer: true, greater_than_or_equal_to: 0, message:"No es un número válido"}

	validates :subject_id,
            presence: { message: "Introduzca la materia del estudiante"}

	validates :student_id,
            presence: { message: "Introduzca el estudiante"}            			      

end
