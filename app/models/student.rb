class Student < ApplicationRecord
  belongs_to :school
  has_many :student_subjects, foreign_key: :student_id, dependent: :destroy
  has_many :subjects, through: :student_subjects

	validates :cedula, 
			presence: {:message  => "Introduzca la cedula del estudiante"},
			format: {with: /\A[A-Za-z0-9\s]+\z/, :message  => "Caracteres invalidos"},
 			numericality: { :message  => "No es un numero valido"},
 			uniqueness: { scope: :cedula, :message => "Cedula ya existe"}

 	validates :nombre,
            presence: { message: "Introduzca el nombre del estudiante" },
			format: {with: /\A([A-Za-z0-9ñÑ\-\.\ ]+)\z/, message: "Caracteres invalidos"}
 	
 	validates :apellido,
            presence: { message: "Introduzca el apellido del estudiante" },
			format: {with: /\A([A-Za-z0-9ñÑ\-\.\ ]+)\z/, message: "Caracteres invalidos"}

	validates :fecha_nac,
            presence: { message: "Introduzca la fecha de nacimiento del estudiante"}

    validates :genero,
            presence: { message: "Introduzca el genero del estudiante"}	

 	validates :school_id,
            presence: { message: "Introduzca el colegio del estudiante"}	           				

  #para los labels de simple_form
  def to_label
  	"#{self.cedula} - #{self.nombre} #{self.apellido}"
  end

end
