class Subject < ApplicationRecord
  has_many :student_subjects, foreign_key: :subject_id, dependent: :destroy
  has_many :students, through: :student_subjects

  validates :nombre,
            presence: { message: "Introduzca el nombre de la materia" },
			format: {with: /\A([A-Za-z0-9ñÑ\-\.\ ]+)\z/, message: "Caracteres invalidos"}
 	
end
